const drawCircle = () => {
    const divs = document.querySelector("#start");
    document.getElementById("clic").style.display = "none";

    const clic = document.createElement("input");
    clic.id = "clic2";
    clic.type = "button";
    clic.onclick = draw;
    clic.value = "Нарисовать";
    divs.insertBefore(clic, divs.firstChild);

    const input = document.createElement("input");
    input.id = "circie";
    divs.insertBefore(input, divs.firstChild);
}
const draw = () => {
    const danCircie = parseInt(document.getElementById("circie").value);

    document.getElementById("clic2").style.display = "none";
    document.getElementById("circie").style.display = "none";

    for (let i = 0; i < danCircie; i++){
        createNewCircie(i, danCircie);
    }
}

const createNewCircie = (i, z) => {
    const divs = document.querySelector("#start");
    const input = document.createElement("input");
    input.type = "button";
    input.id = i;
    input.onclick = removeCircie;
    input.class = "colorButton";
    input.setAttribute("style", "margin:2px; width:"+z+"px; height:"+z+"px; border-radius: "+z+"px; background-color: "+colors()+"");
    divs.append(input);
}

const removeCircie = (id) => {
    const clicButtons = id.currentTarget.id;
    document.getElementById(clicButtons).style.display = "none";
}

function colors(){
    var c='#'+rand()+rand()+rand();
    return c;

};

function rand(){
    var c=parseInt(Math.random()*255).toString(16);
    return (""+c).length==1?'0'+c:c;
};